module.exports = function() {
    $.gulp.task('img:dev', () => {
        return $.gulp.src(['./dev/static/img/**/*.{png,jpg,gif}', '!./dev/static/img/png/*.png'])
            .pipe($.gulp.dest('./build/static/img/'));
    });

    $.gulp.task('img:build', () => {
        return $.gulp.src(['./dev/static/img/**/*.{png,jpg,gif}', '!./dev/static/img/png/*.png'])
            // .pipe($.gp.tinypng('S3MYcaMfBDfjgM9r0ymtIYG8vv9eUApN'))
            .pipe($.gulp.dest('./build/static/img/'));
    });


    $.gulp.task('svg:copy', () => {
        return $.gulp.src('./dev/static/img/general/*.svg')
            .pipe($.gulp.dest('./build/static/img/general/'));
    });
};
