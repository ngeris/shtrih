module.exports = function() {

    $.gulp.task('png', () => {
        let spriteData = $.gulp.src('./dev/static/img/png/*.png')
            .pipe($.spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.scss',
                algorithm: 'binary-tree',
                // cssFormat: 'scss',
                cssTemplate: './dev/static/styles/components/scss.sprite.mustache',
                cssVarMap: function(sprite) {
                    sprite.name = 's-' + sprite.name
                }
            }));
        spriteData.img.pipe($.gulp.dest('./build/static/img/png/'));
        spriteData.css.pipe($.gulp.dest('./dev/static/styles/components/'));
        return spriteData;
    });
};