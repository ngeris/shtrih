(function () {
    /* scroll to block */
    function scrollToDiv (element, navHeight){
        let offset = element.offset(),
            offsetTop = offset.top,
            totalScroll = offsetTop - navHeight;

        /* animate */
        $('body,html').animate({
            scrollTop: totalScroll
        }, 500);
    }

    /* anchor click */
    $('a[href="#anchor"]').on('click', function () {
        let el = $(this).attr('href').slice(1),
            elWrapped = $('[data-anchor=' + el + ']'),
            headerHeight = document.querySelector('.header').clientHeight;

        scrollToDiv(elWrapped, headerHeight);

        return false;
    });
})();

(function () {
    /* mobile menu */
    let btn = document.querySelector('.hamburger'),
        menu = document.querySelector('.js-menu'),
        body = document.querySelector('body');

    if (btn) {
        btn.addEventListener('click', function () {
            this.classList.toggle('is-active');
            $(menu).fadeToggle(300);
            body.classList.toggle('is-hidden');
        });
    }

})();

(function () {
    /* order button change container */
    let orderBtn = document.querySelector('.order-callback__btn'),
        headerMenu = document.querySelector('.header__menu'),
        headerOrderContainer = document.querySelector('.order-callback');

    if(matchMedia) {
        let screen = window.matchMedia('(max-width:480px)');

        screen.addListener(changes);
        changes(screen);
    }

    function changes(screen) {
        if(screen.matches) {
            headerMenu.appendChild(orderBtn);
        } else {
            headerOrderContainer.appendChild(orderBtn);
        }
    }
})();

(function () {
    /* clients slider */
    if (document.querySelector('.slider-clients') !== null) {
        // top slider
        $('.slider-clients__content').slick({
            infinite: true,
            dots: false,
            speed: 400,
            prevArrow: '.slider-clients .slick-arrow-prev',
            nextArrow: '.slider-clients .slick-arrow-next',
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: 2000,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
})();

(function () {
    // examples and modal slider
    if (document.querySelector('.portfolio')) {

        let portfolioItems = document.querySelectorAll('.item-portfolio'),
            portfolioImages;

        /* set data-id to portfolio items */
        for (let i = 0, j = 1; i < portfolioItems.length; ++i, ++j) {
            portfolioItems[i].dataset.id = j;
            /* set data-id to portfolio items img */
            portfolioImages = portfolioItems[i].querySelectorAll('.item-portfolio__gallery img');
            for(let k = 0, l = 1; k < portfolioImages.length; ++k, ++l) {
                portfolioImages[k].dataset.id = l;
            }
        }

        const modal = document.querySelector('.modal-slider');

        // index - the image clicked
        let initSlider = (index,e) => {

            let sliderWrapper = document.querySelector('.modal-slider__wrapper');
            sliderWrapper.innerHTML = '';

            let images = [].slice.call(e.target.nextElementSibling.querySelectorAll(' img'));

            // create and add slides to wrapper slider
            for (let i = 0, j = 1; i < images.length; ++i, ++j) {
                let slide = document.createElement('div'),
                    img = document.createElement('img');
                slide.classList.add('swiper-slide');
                slide.classList.add('modal-slider__slide');
                img.src = images[i].src;
                img.dataset.id = j;
                slide.appendChild(img);
                sliderWrapper.appendChild(slide);
            }

            modal.hidden = false;

            const modalSlider = new Swiper('.modal-slider__container', {
                speed: 400,
                loop: true,
                centeredSlides: true,
                simulateTouch: true,
                nextButton: '.modal-slider__arrow--next',
                prevButton: '.modal-slider__arrow--prev',
                keyboardControl: true,
                slideActiveClass: 'modal-slider__slide--active'
            });

            // go to current slide
            modalSlider.slideTo(index, 0);

            // set current and amount numbers
            let current = document.querySelector('.modal-slider__current');
            current.textContent = index;
            document.querySelector('.modal-slider__amount').textContent = images.length;


            modalSlider.on('slideChangeStart', () => {
                current.textContent = document.querySelector('.modal-slider__slide--active').querySelector('img').dataset.id;
            });

            modalSlider.on('sliderMove', () => {
                current.textContent = document.querySelector('.modal-slider__slide--active').querySelector('img').dataset.id;
            });

            // destroy slider
            document.querySelector('.modal-slider__close').addEventListener('click', () => {
                modal.hidden = true;
                modalSlider.destroy(false, true);
            });

            document.querySelector('.modal-slider__overlay').addEventListener('click', () => {
                modal.hidden = true;
                modalSlider.destroy(false, true);
            });

            document.documentElement.addEventListener('keydown', (e) => {
                if (e.keyCode === 27) {
                    modal.hidden = true;
                    modalSlider.destroy(false, true);
                }
            });
        };

        // init modal slider
        document.querySelector('.portfolio').addEventListener('click', (e) => {
            if (e.target.classList.contains('js-sl-popup')) {
                let id = e.target.nextElementSibling.querySelector('img').dataset.id;
                initSlider(id,e);
            }
        });

    }
})();

(function () {

    /* helper for phone methods */
    $.validator.addMethod('minlenghtphone', function (value) {
        return value.replace(/\D+/g, '').length > 11;
    }, 'Введите полный номер');
    $.validator.addMethod('requiredphone', function (value) {
        return value.replace(/\D+/g, '').length > 1;
    }, 'Это поле обязательно для заполнения');

    /* phone mask */
    $('input[name=b-phone]').inputmask('+38 (999)-999-99-99');

    $('.js-modal-callback-btn').magnificPopup({
        type:'inline'
    });

    /* jquery validate for callback main form */
    $('.m-callback').validate({
        rules: {
            name: {required: true, minlength: 2},
            'b-phone': {requiredphone: true, minlenghtphone: true}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимально 2 символа'}
        },
        submitHandler: function (form) {

            let inputs = form.querySelectorAll('input');

            for (let i = 0; i < inputs.length; i++) {
                inputs[i].value = '';
            }

            $.magnificPopup.open({
                items: {
                    src: '#cb-window-answer'
                },
                type: 'inline',
            });

            $(document).on('click', 'mfp-close', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
        }
    });

    /* jquery validate contacts page form */
    $('#cb-form').validate({
        rules: {
            name: {required: true, minlength: 2},
            'b-phone': {requiredphone: true, minlenghtphone: true}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимально 2 символа'}
        },
        submitHandler: function (form) {
            let submit = form.querySelector('button[type=submit]'),
                inputs = form.querySelectorAll('input');

            for (let i = 0; i < inputs.length; i++) {
                inputs[i].value = '';
            }

            $(submit).magnificPopup({
                items: {
                    src: '#cb-window-answer'
                },
                type: 'inline',
            });

            $(submit).trigger('click');

            $(document).on('click', 'mfp-close', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
        }
    });

    /* subscribe form validate */
    $('#subscribe-form').validate({
        rules: {
            name: {required: true, minlength: 2},
            mail: {required: true, email: true}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимально 2 символа'},
            mail: {required: 'Это поле обязательно для заполнения', email: 'Введите корректную почту'}
        },
    });


})();

(function () {
    function clippingText(selector, length) {
        let titles = document.querySelectorAll(selector);
        for (let selector of titles) {
            if (selector.textContent.split('').length > 100)
                selector.textContent = selector.textContent.slice(0, length) + ' ...';
        }
    }
    clippingText('.services-grid__desc', 115);
})();



(function () {
    // tabs
    function tabs(elems) {
        let tabs = document.querySelectorAll(elems),
            titleClass = elems + '__title',
            panelClass = elems + '__panel';

        let _loop = function _loop(_i3) {
            let titles = tabs[_i3].querySelectorAll(titleClass);
            let panels = tabs[_i3].querySelectorAll(panelClass);

            let _loop2 = function _loop2(_i4) {
                titles[_i4].addEventListener('click', function () {
                    for (let _i5 = 0; _i5 < titles.length; ++_i5) {
                        titles[_i5].classList.remove(titleClass.slice(1) + '--active');
                        panels[_i5].classList.remove(panelClass.slice(1) + '--active');
                    }
                    titles[_i4].classList.add(titleClass.slice(1) + '--active');
                    panels[_i4].classList.add(panelClass.slice(1) + '--active');
                });
            };

            for (let _i4 = 0; _i4 < titles.length; ++_i4) {
                _loop2(_i4);
            }
        };

        for (let _i3 = 0; _i3 < tabs.length; ++_i3) {
            _loop(_i3);
        }
    }

    if (document.querySelector('.tabs')) {
        tabs('.tabs');
    }
}());

(function () {
    // body offset for position absolute header
    function bodyOffset() {
        let page = document.querySelector('.page__wrapper'),
            headerHeight = document.querySelector('.header').clientHeight;

        if(!document.querySelector('.first-screen')) {
            page.style.paddingTop = headerHeight + 'px';
        }
    }
    bodyOffset();
    window.addEventListener('resize',bodyOffset);
})();

(function () {
    $('.accordion__question').on('click', function (e) {
        e.preventDefault();

        let $this = $(this),
            item = $this.closest('.accordion__set'),
            list = $this.closest('.accordion'),
            items = list.find('.accordion__set'),
            content = item.find('.accordion__content'),
            otherContent = list.find('.accordion__content'),
            duration = 200;

        if (!item.hasClass('is-open')) {
            items.removeClass('is-open');
            item.addClass('is-open');
            otherContent.stop(true, true).slideUp(duration);
            content.slideDown(duration);
        } else {
            content.stop(true, true).slideUp(duration);
            item.removeClass('is-open');
        }
    });
})();

window.addEventListener('load', function () {
    // Window load event used just in case window height is dependant upon images
    let footerHeight = 0,
        footerTop = 0,
        footer = document.querySelector('.footer');

    function positionFooter() {

        footerHeight = footer.clientHeight;
        footerTop = (window.pageYOffset + window.innerHeight - footerHeight) + 'px';

        /* DEBUGGING STUFF
        console.log("Document height: ", document.body.clientHeight);
        console.log("Window height: ", window.innerHeight);
        console.log("Window scroll: ", window.pageYOffset);
        console.log("Footer height: ", footerHeight);
        console.log("Footer top: ", footerTop);
        console.log("-----------");
        */

        if ( (document.body.clientHeight + footerHeight) < window.innerHeight) {
            $(footer).css({
                position: 'absolute'
            }).stop().animate({
                top: footerTop
            });
        } else {
            footer.style.position = 'static';
        }
    }

    positionFooter();

    window.addEventListener('scroll', positionFooter);
    window.addEventListener('resize', positionFooter);

});